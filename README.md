# Abstract

This framework provides boilerplate code to write CLIs (Command Line Interfaces).


# Features

* Extendable command set utilizing a plug-in architecture
* Customizable launcher application


# How-To

* Develop: Implement the `de.quipsy.cli.spi.Command` interface for each wanted CLI command.
* Deploy: bundle commands in plug-ins: Put them in a JAR, and register them in the file `META-INF/services/de.quipsy.cli.spi.Command`.
* Execute: Put both, framework and plug-in JARs on the classpath, then invoke `de.quipsy.cli.CLI` the launcher main class.
* Extend: Implement the `de.quipsy.cli.spi.Application` interface to customize the launcher, then provide a plug-in with this implementation being registered in the file `META-INF/services/de.quipsy.cli.spi.Application`.
* Build: `mvn install`. Optionally provide `MAVEN_SETTINGS` and `DEPLOY` when using Gitlab CI/CD.

## Build Notes
* JCommander is used as a project dependency. As *not all* versions of JCommander are found on [Maven Central Repository](https://search.maven.org/artifact/com.beust/jcommander), please add [JCenter](https://bintray.com/cbeust/maven/jcommander) to your [`settings.xml`](https://maven.apache.org/settings.html) configuration:
```xml
<repository>
  <id>jcenter</id>
  <url>https://jcenter.bintray.com</url>
  <snapshots>
    <enabled>false</enabled>
  </snapshots>
</repository>
```
